﻿using System;
using System.Threading.Tasks;
using Messenger.Client.Objects;
using Messenger.Client.Objects.Enums;

namespace Messenger.Client.Services
{
    public interface IMessengerMessageSender
    {
        Task<MessengerResponse> SendResponseAsync(MessengerMessage message, MessengerUser recipient);
        Task<MessengerResponse> SendUpdateAsync(MessengerMessage message, MessengerUser recipient);
        Task<MessengerResponse> SendMessageTagAsync(MessengerMessage message, MessengerUser recipient, MessengerMessageTag tag);

        Task<MessengerResponse> SendResponseAsync(MessengerMessage message, MessengerUser recipient, String accessToken);
        Task<MessengerResponse> SendUpdateAsync(MessengerMessage message, MessengerUser recipient, String accessToken);
        Task<MessengerResponse> SendMessageTagAsync(MessengerMessage message, MessengerUser recipient, MessengerMessageTag tag, String accessToken);

        Task<MessengerResponse> SetSenderActionAsync(MessengerSenderAction senderAction, MessengerUser recipient);
        
        Task<MessengerResponse> SetSenderActionAsync(MessengerSenderAction senderAction, MessengerUser recipient, String accessToken);
    }
}
