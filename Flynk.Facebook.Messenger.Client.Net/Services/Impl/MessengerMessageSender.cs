using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Messenger.Client.Config;
using Messenger.Client.Errors;
using Messenger.Client.Objects;
using Messenger.Client.Objects.Enums;
using Messenger.Client.Utilities;

namespace Messenger.Client.Services.Impl
{
    public class MessengerMessageSender : IMessengerMessageSender
    {
        private const String UrlTemplate = "https://graph.facebook.com/v2.6/me/messages?access_token={0}";

        private readonly HttpClient client;
        private readonly IMessengerSerializer serializer;

        public MessengerMessageSender(IMessengerSerializer serializer)
        {
            this.client = new HttpClient();
            this.serializer = serializer;
        }

        public Task<MessengerResponse> SendResponseAsync(MessengerMessage message, MessengerUser recipient)
        {
            return SendResponseAsync(message, recipient, MessengerConfig.AccessToken);
        }

        public Task<MessengerResponse> SendResponseAsync(MessengerMessage message, MessengerUser recipient, string accessToken)
        {
            var request = new MessengerSendMessageRequest { Recipient = recipient, Message = message, MessagingType = MessengerMessagingType.Response };
            return SendMessage(request, accessToken);
        }

        public Task<MessengerResponse> SendUpdateAsync(MessengerMessage message, MessengerUser recipient)
        {
            return SendUpdateAsync(message, recipient, MessengerConfig.AccessToken);
        }

        public Task<MessengerResponse> SendUpdateAsync(MessengerMessage message, MessengerUser recipient, string accessToken)
        {
            var request = new MessengerSendMessageRequest { Recipient = recipient, Message = message, MessagingType = MessengerMessagingType.Update };
            return SendMessage(request, accessToken);
        }

        public Task<MessengerResponse> SendMessageTagAsync(MessengerMessage message, MessengerUser recipient, MessengerMessageTag tag)
        {
            return SendMessageTagAsync(message, recipient, tag, MessengerConfig.AccessToken);
        }

        public Task<MessengerResponse> SendMessageTagAsync(MessengerMessage message, MessengerUser recipient, MessengerMessageTag tag, string accessToken)
        {
            var request = new MessengerSendMessageRequest { Recipient = recipient, Message = message, MessagingType = MessengerMessagingType.MessageTag, Tag = tag };
            return SendMessage(request, accessToken);
        }

        public Task<MessengerResponse> SetSenderActionAsync(MessengerSenderAction senderAction, MessengerUser recipient)
        {
            return SetSenderActionAsync(senderAction, recipient, MessengerConfig.AccessToken);
        }

        public Task<MessengerResponse> SetSenderActionAsync(MessengerSenderAction senderAction, MessengerUser recipient, string accessToken)
        {
            var request = new MessengerSenderActionRequest { Recipient = recipient, SenderAction = senderAction };

            return SendMessage(request, accessToken);
        }

        private async Task<MessengerResponse> SendMessage<TRequest>(TRequest request, string accessToken)
        {
            var url = String.Format(UrlTemplate, accessToken);

            var content = new StringContent(serializer.Serialize(request));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            try
            {
                var response = await client.PostAsync(url, content);
                var result = new MessengerResponse
                {
                    Succeed = response.IsSuccessStatusCode,
                    RawResponse = await response.Content.ReadAsStringAsync()
                };

                return result;
            }
            catch (Exception exc)
            {
                throw new MessengerException(exc);
            }
        }

    }
}
