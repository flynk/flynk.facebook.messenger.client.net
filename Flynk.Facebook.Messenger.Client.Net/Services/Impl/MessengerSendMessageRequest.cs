﻿using Messenger.Client.Objects;
using Messenger.Client.Objects.Enums;
using Newtonsoft.Json;

namespace Messenger.Client.Services.Impl
{
    internal class MessengerSendMessageRequest
    {
        [JsonProperty("messaging_type")]
        public MessengerMessagingType MessagingType { get; set; }

        public MessengerMessageTag Tag { get; set; }

        public MessengerUser Recipient { get; set; }

        public MessengerMessage Message { get; set; }
    }
}
