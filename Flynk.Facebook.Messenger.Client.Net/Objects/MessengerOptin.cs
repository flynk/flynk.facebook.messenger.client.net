﻿using Newtonsoft.Json;
using System;

namespace Messenger.Client.Objects
{
    public class MessengerOptin
    {
        public String Ref { get; set; }

        [JsonProperty("user_ref")]
        public String UserRef { get; set; }
    }
}
