﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Messenger.Client.Objects.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MessengerMessagingType
    {
        [EnumMember(Value = "RESPONSE")]
        Response,
        [EnumMember(Value = "UPDATE")]
        Update,
        [EnumMember(Value = "MESSAGE_TAG")]
        MessageTag,
    }
}
