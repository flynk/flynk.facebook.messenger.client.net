﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Messenger.Client.Objects.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MessengerMessageTag
    {
        [EnumMember(Value = "COMMUNITY_ALERT")]
        CommunityAlert,
        [EnumMember(Value = "CONFIRMED_EVENT_REMINDER")]
        ConfirmedEventReminder,
        [EnumMember(Value = "NON_PROMOTIONAL_SUBSCRIPTION")]
        NonPromotionalSubscription,
        [EnumMember(Value = "PAIRING_UPDATE")]
        PairingUpdate,
        [EnumMember(Value = "APPLICATION_UPDATE")]
        ApplicationUpdate,
        [EnumMember(Value = "ACCOUNT_UPDATE")]
        AccountUpdate,
        [EnumMember(Value = "PAYMENT_UPDATE")]
        PaymentUpdate,
        [EnumMember(Value = "PERSONAL_FINANCE_UPDATE")]
        PersonalFinanceUpdate,
        [EnumMember(Value = "SHIPPING_UPDATE")]
        ShippingUpdate,
        [EnumMember(Value = "RESERVATION_UPDATE")]
        ReservationUpdate,
        [EnumMember(Value = "ISSUE_RESOLUTION")]
        IssueResolution,
        [EnumMember(Value = "APPOINTMENT_UPDATE")]
        AppointmentUpdate,
        [EnumMember(Value = "GAME_EVENT")]
        GameEvent,
        [EnumMember(Value = "TRANSPORTATION_UPDATE")]
        TransportationUpdate,
        [EnumMember(Value = "FEATURE_FUNCTIONALITY_UPDATE")]
        FeatureFunctionalityUpdate,
        [EnumMember(Value = "TICKET_UPDATE")]
        TicketUpdate
    }
}
